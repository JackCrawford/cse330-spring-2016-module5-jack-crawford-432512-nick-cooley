<!DOCTYPE html>
	<head>
		<meta charset="utf-8"/>
		<title>Calendar</title>
		<link rel="stylesheet" type="text/css" href="calStyle.css">
		<style>
		</style>
	</head>
	<body style="width: 1200px">
		<div id="title" style= "margin: 0 auto">
			<h2 style="text-align: center">Calendar</h2>
		</div>
		<br>
		<div id="header" style="margin: 0 auto">
			<button type="button" id="login">Login</button>
			<button type="button" id="signUp">Sign Up</button>
			<button type="button" id="share">Share!</button>
		</div>
		<br>
		<div id="outer" style="margin: 0 auto">
			<div id="Sunday" class="day">
				<br>
				<p style="text-align: center">Sunday</p>
			</div>
			<div id="Monday" class="day">
				<br>
				<p style="text-align: center">Monday</p>
			</div>
			<div id="Tuesday" class="day">
				<br>
				<p style="text-align: center">Tuesday</p>
			</div>
			<div id="Wednesday" class="day">
				<br>
				<p style="text-align: center">Wednesday</p>
			</div>
			<div id="Thursday" class="day">
				<br>
				<p style="text-align: center">Thursday</p>
			</div>
			<div id="Friday" class="day">
				<br>
				<p style="text-align: center">Friday</p>
			</div>
			<div id="Saturday" class="day">
				<br>
				<p style="text-align: center">Saturday</p>
			</div>
			<div id="inner1" class="row1"></div>
			<div id="inner2" class="row1"></div>
			<div id="inner3" class="row1"></div>
			<div id="inner4" class="row1"></div>
			<div id="inner5" class="row1"></div>
			<div id="inner6" class="row1"></div>
			<div id="inner7" class="row1"></div>
			<div id="inner8" class="row2"></div>
			<div id="inner9" class="row2"></div>
			<div id="inner10" class="row2"></div>
			<div id="inner11" class="row2"></div>
			<div id="inner12" class="row2"></div>
			<div id="inner13" class="row2"></div>
			<div id="inner14" class="row2"></div>
			<div id="inner15" class="row3"></div>
			<div id="inner16" class="row3"></div>
			<div id="inner17" class="row3"></div>
			<div id="inner18" class="row3"></div>
			<div id="inner19" class="row3"></div>
			<div id="inner20" class="row3"></div>
			<div id="inner21" class="row3"></div>
			<div id="inner22" class="row4"></div>
			<div id="inner23" class="row4"></div>
			<div id="inner24" class="row4"></div>
			<div id="inner25" class="row4"></div>
			<div id="inner26" class="row4"></div>
			<div id="inner27" class="row4"></div>
			<div id="inner28" class="row4"></div>
			<div id="inner29" class="row5"></div>
			<div id="inner30" class="row5"></div>
			<div id="inner31" class="row5"></div>
			<div id="inner32" class="row5"></div>
			<div id="inner33" class="row5"></div>
			<div id="inner34" class="row5"></div>
			<div id="inner35" class="row5"></div>
			<div id="inner36" class="row6"></div>
			<div id="inner37" class="row6"></div>
			<div id="inner38" class="row6"></div>
			<div id="inner39" class="row6"></div>
			<div id="inner40" class="row6"></div>
			<div id="inner41" class="row6"></div>
			<div id="inner42" class="row6"></div>
		</div></body>
</html>