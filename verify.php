<?php
ini_set("session.cookie_httponly", 1);
$user = $_POST['username'];
$pwd_guess = $_POST['password'];
require 'database.php';

$stmt = $mysqli->prepare("SELECT COUNT(*), username, password FROM users WHERE username=?");
if(!$stmt){
        printf("Query Prep Failed: %s\n", $mysqli->error);
        exit;
}
$stmt->bind_param('s', $user);
$stmt->execute();
$stmt->bind_result($cnt, $user_id, $pwd_hash);
$stmt->fetch();
$data = array();
if($cnt == 1 && crypt($pwd_guess, $pwd_hash) == $pwd_hash){
	$response = 1;
	session_start();
	$_SESSION["user"] = $user;
        $_SESSION["token"] = substr(md5(rand()), 0, 10);
}
else{
	$response = 0;
}
$data['response'] = $response;
echo json_encode($data);
?>