<?php
ini_set("session.cookie_httponly", 1);
session_start();
require 'database.php';
$day = $_POST['day'];
$month = $_POST['month'];
$year = $_POST['year'];
$date = $month . "-" . $day . "-" . $year;
$eventName = $_POST['name'];
$data = array();
if(isset($_SESSION['user'])){
	$stm = $mysqli->prepare("SELECT COUNT(*) FROM events where user=? and name=? and date=?");
	if(!$stm){
		printf("Query Prep Failed: %s\n", $mysqli->error);
		exit;
	}
	$stm->bind_param('sss', $_SESSION['user'], $eventName, $date);
	$stm->execute();
	$stm->bind_result($cnt);
	$stm->fetch();
	$stm->close();
	if($cnt != 0){
		$stmt = $mysqli->prepare("DELETE FROM events WHERE user=? and name=? and date=?");
		if(!$stmt){
			printf("Query Prep Failed: %s\n", $mysqli->error);
			exit;
		}
		$stmt->bind_param('sss', $_SESSION['user'], $eventName, $date);
		$stmt->execute();
		$data['response'] = 1;
	}
	else{
		$data['response'] = 0;
	}
}
else{
	$data['response'] = -1;
}
echo json_encode($data);
?>