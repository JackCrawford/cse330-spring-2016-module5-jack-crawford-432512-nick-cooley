<?php
ini_set("session.cookie_httponly", 1);
$user = $_POST["username"];
$password = $_POST["password"];
require 'database.php';
$stm = $mysqli->prepare("SELECT COUNT(*) FROM users WHERE username=?");
$stm->bind_param('s', $user);
$stm->execute();
$stm->bind_result($count);
$stm->fetch();
$stm->close();
$data = array();
if($count < 1){
	$salt = uniqid(mt_rand(), true);
	$hash = crypt($password, $salt);
	$stmt = $mysqli->prepare("insert into users (name, pass) values (?, ?)");
	if(!$stmt){
		printf("Query Prep Failed: %s\n", $mysqli->error);
	        exit;
	}
	$stmt->bind_param('ss', $user, $hash);
	$stmt->execute();
	$stmt->close();
	$response = 1;
}
else{
	$response = 0;
}
$data['response'] = $response;
echo json_encode($data);
?>