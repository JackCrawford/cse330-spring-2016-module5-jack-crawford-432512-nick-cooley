<?php
ini_set("session.cookie_httponly", 1);
$msg = $_POST['msg'];
$data = array();
if($msg == "logout"){
	session_start();
	session_destroy();
	$data['response'] = 1;
}
else{
	$data['response'] = 0;
}
echo json_encode($data);
?>