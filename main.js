var main = function(){

    var d = new Date();
    var n = d.getMonth();
    var y = d.getFullYear();
    var dayOfWeek = d.getDay();
    var dayOfMonth = d.getDate();
    
    var curr = (dayOfMonth-1);
    var startDay = 2;
    var endDay = 0;
    
    function logInUser(){
        var usr = document.getElementById("username").value;
        var pwd = document.getElementById("password").value;
        $.ajax({
            type: "post",
            url: "verify.php",
            data: {username: usr, password: pwd},
            success: function(data){
                var json = JSON.parse(data);
                if(json.response == 1){
                    var parent = document.getElementById("loginDiv");
                    while(parent.hasChildNodes()){
                        parent.removeChild(parent.lastChild);
                    }
                    parent = document.getElementById("signupDiv");
                    while(parent.hasChildNodes()){
                        parent.removeChild(parent.lastChild);
                    }
                    createCalendar(currentMonth);
                }
                else{
                    alert("login failure");
                }
            }
        });
    }
    function logout(){
        $.ajax({
            type: "post",
            url: "logout.php",
            data: {msg: "logout"},
            success: function(data){
                var json = JSON.parse(data);
                if(json.response == 1){
                    clear();
                    clearCalendar();
                    startUp();
                }
                else{
                    alert("error in logout");
                }
            }
        });
    }
    
    
    function signUp(){
        var desiredUsr = document.getElementById("desiredUsername").value;
        var desiredPwd = document.getElementById("desiredPassword").value;
        $.ajax({
            type: "post",
            url: "create.php",
            data: {username: desiredUsr, password: desiredPwd},
            success: function(data){
                var json = JSON.parse(data);
                if(json.response == 1){
                    var parent = document.getElementById("signupDiv");
                    while(parent.hasChildNodes()){
                        parent.removeChild(parent.lastChild);
                    }
                    alert("successfully added");
                }
                else{
                    alert("signup failure");
                }
            }
        });
    }
    
    function showEvent(){ //PHP-->JS
    
        //should request and receive JSON info and convert it into javascript object, which it then appends to HTML document
        $.ajax({
            type: "post",
            url: "events.php",
            data: {day: String(d), month: String(m), year: String(y)},
            success: function(data){
                clearCalendar();
                var json = jQuery.parseJSON(data);
                var events = document.getElementById("events");
                var eventTable = document.createElement("table");
                var z = 0;
                eventTable.setAttribute("id", "eventTable");
                eventTable.setAttribute("border", "1");
                events.appendChild(eventTable);
                $.each(json, function(key, value){
                        var newRow = document.createElement("tr");
                        newRow.setAttribute("id", "row" + z);
                        eventTable.appendChild(newRow);
                        $.each(value, function(key, value){
                            var rowParent = document.getElementById("row" + z);
                            if(value == "personal"){
                                rowParent.style.color = "blue";
                            }
                            if(value == "business"){
                                rowParent.style.color = "green";
                            }
                            if(value == "school"){
                                rowParent.style.color = "red";
                            }
                            if(value == "other"){
                                rowParent.style.color = "black";
                            }
                            var cell = document.createElement("td");
                            var content = document.createTextNode(value);
                            cell.appendChild(content);
                            rowParent.appendChild(cell);
                        });
                        z++;
                });
                var parent = document.getElementById("buttons");
                var rmInput = document.createElement("input");
                rmInput.setAttribute("id", "rmEvent");
                rmInput.setAttribute("type", "text");
                var rmLabel = document.createElement("label");
                rmLabel.appendChild(document.createTextNode("Name of Event to Remove: "));
                rmLabel.appendChild(rmInput);
                parent.appendChild(rmLabel);
                var rmBtn = document.createElement("button");
                rmBtn.setAttribute("id", "rmBtn");  
                rmBtn.setAttribute("type", "button");   
                rmBtn.setAttribute("onClick", "removeEvent(" + d + ", " + m + ", " + y + ")");
                parent.appendChild(rmBtn);
                parent.appendChild(document.createElement("br"));
                var addInputName = document.createElement("input");
                var addInputTime = document.createElement("input");
                var addInputType = document.createElement("input");
                addInputName.setAttribute("id", "addEventName");
                addInputName.setAttribute("type", "text");
                addInputTime.setAttribute("id", "addEventTime");
                addInputTime.setAttribute("type", "text");
                addInputType.setAttribute("id", "addEventType");
                addInputType.setAttribute("type", "text");
                var addLabelName = document.createElement("label");
                var addLabelTime = document.createElement("label");
                var addLabelType = document.createElement("label");
                addLabelName.appendChild(document.createTextNode("Name of Event to Add: "));
                addLabelTime.appendChild(document.createTextNode("Time of Event to Add: "));
                addLabelType.appendChild(document.createTextNode("Type of Event to Add (personal, business, school, or other): "));
                addLabelName.appendChild(addInputName);
                addLabelTime.appendChild(addInputTime);
                addLabelType.appendChild(addInputType);
                parent.appendChild(addLabelName);
                parent.appendChild(addLabelTime);
                parent.appendChild(addLabelType);
                var addBtn = document.createElement("button");
                addBtn.setAttribute("id", "addBtn");
                addBtn.setAttribute("type", "button");
                addBtn.setAttribute("onClick", "addEvent(" + d + ", " + m + ", " + y + ")");
                parent.appendChild(addBtn);
                parent.appendChild(document.createElement("br"));
                var modInputName = document.createElement("input");
                var modInputProp = document.createElement("input");
                var modInputVal = document.createElement("input");
                modInputName.setAttribute("id", "modInputName");
                modInputName.setAttribute("type", "text");
                modInputProp.setAttribute("id", "modInputProp");
                modInputProp.setAttribute("type", "text");
                modInputVal.setAttribute("id", "modInputVal");
                modInputVal.setAttribute("type", "text");
                var modLabelName = document.createElement("label");
                var modLabelProp = document.createElement("label");
                var modLabelVal = document.createElement("label");
                modLabelName.appendChild(document.createTextNode("Name of Event to Modify: "));
                modLabelProp.appendChild(document.createTextNode("Property of Event to Modify: "));
                modLabelVal.appendChild(document.createTextNode("New Value for Property being Modified: "));
                modLabelName.appendChild(modInputName);
                modLabelProp.appendChild(modInputProp);
                modLabelVal.appendChild(modInputVal);
                parent.appendChild(modLabelName);
                parent.appendChild(modLabelProp);
                parent.appendChild(modLabelVal);
                var modBtn = document.createElement("button");
                modBtn.setAttribute("id", "modBtn");
                modBtn.setAttribute("type", "button");
                modBtn.setAttribute("onClick", "modifyEvent(" + d + ", " + m + ", " + y + ")");
                modBtn.vale = "Modify";
                parent.appendChild(modBtn);
                var backBtn = document.createElement("button");
                backBtn.setAttribute("id", "backBtn");
                backBtn.setAttribute("type", "button");
                backBtn.setAttribute("value", "Back to Calendar");
                backBtn.setAttribute("onClick", "createCalendar(currentMonth)");
                parent.appendChild(document.createElement("br"));
                parent.appendChild(backBtn);
            }
        });
    }
    
    //function refreshEvents(month, year){ //PHP-->JS
    
        //should call showEvent() in FOR loop
    //}
    
    function addEvent(eventName, eventDay, eventTime, eventYear, eventID){
        
        //will send new event and its properties to PHP/MySQL
        
        $.ajax({
                        type: "post",
                        url: "addEvent.php",
                        data: {day: String(eventDay), year: String(eventYear), name: String(eventName), time: String(eventTime), id: String(eventID)},
            success: function(data){
                var json = JSON.parse(data);
                if(json.response == 1){
                    alert("event successfully added");
                }
                else{
                    alert("error in addition");
                }   
            }
        });
    }
    
    function deleteEvent(eventID){
    
        //will send data to PHP that MySQL event should be deleted
        var eventName = document.getElementById("rmEvent").value;
        $.ajax({
            type: "post",
            url: "remove.php",
            data: {day: String(d), month: String(m), year: String(y), name: String(eventName)},
            success: function(data){
                var json = JSON.parse(data);
                if(json.response == 1){
                    var parent = document.getElementById("buttons");
                    while(parent.hasChildNodes()){
                        parent.removeChild(parent.lastChild);
                    }
                    parent = document.getElementById("events");
                                    while(parent.hasChildNodes()){
                                            parent.removeChild(parent.lastChild);
                                    }
                    getEvents(d, m, y);
                }
                if(json.response === 0){
                    alert("event doesn't exist");
                }
                if(json.response == -1){
                    alert("you are not logged in as a user");
                }
            }
        });
    }

    
    
    function daysInMonth(month,year) {
            return new Date(year, month, 0).getDate();
        }
    
    var currDaysMonth = daysInMonth(n+1, y+1);
    
    endDay = startDay+(currDaysMonth%7);
    
    if (endDay>6) {
        endDay = 0;
    }
    
    var displayMonth = function(n, y){
        if (n===11) {
            $("#date").html("December "+y);
        }
        else if (n===10) {
            $("#date").html("November "+y);
        }
        else if (n===9) {
            $("#date").html("October "+y);
        }
        else if (n===8) {
            $("#date").html("September "+y);
        }
        else if (n===7) {
            $("#date").html("August "+y);
        }
        else if (n===6) {
            $("#date").html("July "+y);
        }
        else if (n===5) {
            $("#date").html("June "+y);
        }
        else if (n===4) {
            $("#date").html("May "+y);
        }
        else if (n===3) {
            $("#date").html("April "+y);
        }
        else if (n===2) {
            $("#date").html("March "+y);
        }
        else if (n===1) {
            $("#date").html("February "+y);
        }
        else{
            $("#date").html("January "+y);
        }

    }
    
    var loadDates = $(document).ready(function(){
        
        displayMonth(n, y);
        
        for(i=1; i<=currDaysMonth; ++i){
            $("#inner"+(i+startDay)).html(i);
        }
        //Also need a function that loads in events from PHP data for that month and user
    });
    
    //loadEvents should be its own function that you can use in the loadDates, next, groups functions
    //it should the date as a parameter
    //inside the function, it will access events database from PHP and MySql and return an array of arrays
    //createEvent function will send data to PHP and MySql, along with creating a new event box on the browser
    //it will also expand the relative div row vertically
    //delete event will remove the event from the database, shrink the div row, then
    //remove the html element for the actual browser by calling loadEvents with the updated information
    
    var next = $("#next").click(function(){ //next month button
        
        for (i=1; i<=42; ++i){
            $("#inner"+(i+startDay)).html(""); //clears previous days
        }
        
        n = n+1;
        if (n>11) {
            n=0
            y=y+1;
        }
        
        displayMonth(n, y);
        
        currDaysMonth = daysInMonth(n+1, y);
       
    
        startDay=endDay;
        
    
        for(i=1; i<=currDaysMonth; ++i){
            $("#inner"+(i+startDay)).html(i);
        }
        
        endDay = (startDay+currDaysMonth)%7;
        
    });
    
    $(document).ready(next);
    
    var prev = $("#prev").click(function(){ //previous month button
        
        for (i=1; i<=42; ++i){
            $("#inner"+(i+startDay)).html(""); //clears previous days
        }
        
        n = n-1;
        if (n<0) {
            n=11
            y=y-1;
        }
        
        displayMonth(n, y);
        
        currDaysMonth = daysInMonth(n+1, y);
        
        endDay=startDay;
        
        startDay=endDay-(currDaysMonth%7);
        
        if (startDay<0) {
            startDay=startDay+7;
        }
    
        for(i=1; i<=currDaysMonth; ++i){
            $("#inner"+(i+startDay)).html(i);
        }
        
        endDay = (startDay+currDaysMonth)%7;
        
    });
    
    $(document).ready(prev);

    var addEvent = function(){
        
        currDaysMonth=daysInMonth(n+1, y)
        
        var $div = $("<div class = 'container'><div>")
        var $event = $("<input type='text' id='event' class='elementInput' placeholder='Event' style='width:90px'><br>");
        var $time = $("<input type='text' id='time' class='elementInput' placeholder='Time' style='width:90px'><br>")
        var $day = $("<input type='text' id='day' class='elementInput' placeholder='Day of Month' style='width:90px'><br>")
        var $tag = $("<input type='text' id='tag' class='elementInput' placeholder='Tag Name' style='width:90px'><br>")
        var $save = $("<button type='button' class='save'>Save</button>")
        

            
        $("#createEvent").click(function(){
                
                
            $($div).insertAfter("#createEvent");
            $($div).append($event);
            $($div).append($time);
            $($div).append($day);
            $($div).append($tag);
            $($div).append($save);
                
        });
            
        
        $($save).click(function(){
            var eventInput = document.getElementById("event").value;
            var eventTime = document.getElementById("time").value;
            var eventDay = (parseInt(document.getElementById("day").value))+startDay;
            var tagName = document.getElementById("tag").value;
            var $delete = $("<button type='button' class='delete'>Delete</button>");
            var $eventContainer = $("<div></div>");
            
            $("#inner"+eventDay).append($eventContainer);
            $($eventContainer).append(eventInput, "<br>");
            $($eventContainer).append(eventTime, "<br>");
            $($eventContainer).append(tagName, "<br>");
            $($eventContainer).append($delete, "<br>");
            
            var $rowString = $("#inner"+eventDay).attr("class");
            var row = parseInt($rowString.substring(3));
            
            var $height = parseInt($(".row"+row).css('height'));
            $(".row"+row).css("height", $height+70);
    
            var deleteEvent = function(){
                $($delete).click(function(){
                    $(this).parent().remove();
                    var $rowString = $("#inner"+eventDay).attr("class");
                    var row = parseInt($rowString.substring(3));
            
                    var $height = parseInt($(".row"+row).css('height'));
                    $(".row"+row).css("height", $height-70);
                });
            }
            
            $(document).ready(deleteEvent);
    
        });    
        
    }
    
    $(document).ready(addEvent);
    
}


$(document).ready(main);