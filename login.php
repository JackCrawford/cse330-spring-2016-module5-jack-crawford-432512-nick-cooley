<?php
		header("Content-Type: application/json");
		$mysqli = new mysqli('localhost','root','Happypooter20!','cal');
		if($mysqli->connect_errno) {
			printf("Connection Failed: %s\n", $mysqli->connect_error);
			exit;
		}

		//Declare user and pass variables
		$username = $_POST['loginUsername'];
		$password = $_POST['password'];
		$cryptpw = crypt($password,"cooleycraw");

		//Check to see if valid login credentials were entered

		$stmt = $mysqli->prepare("select name, pass, first, last from users where name=? and pass=?");
		if(!$stmt){
			printf("Query Prep Failed: %s\n", $mysqli->error);
			exit;
		}
		$stmt->bind_param('ss',$username,$cryptpw);
		$stmt->execute();
		$stmt->bind_result($user,$cryptpass,$first,$last);
		while($stmt->fetch()) {
			if(isset($user)) {
				session_start();
				$_SESSION['loginUsername'] = $username;
				$_SESSION['token'] = substr(md5(rand()), 0, 10);

				echo json_encode(array(
					"success" => true
					));
				exit;
			}
			else {
				echo json_encode(array(
					"success" => false,
					"message" => "Incorrect Username or Password. If you are a new user, please sign up to the left. "
					));
				exit;
			}
		}
?>